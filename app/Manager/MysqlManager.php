<?php

namespace App\Manager;

use PDO;

class MysqlManager
{
    /**
     * Connect to the database
     *
     * @return PDO
     */
    public function connect(): PDO
    {
        $host = $_SERVER['MYSQL_HOST'] ?? null;
        $user = $_SERVER['MYSQL_USER'] ?? null;
        $password = $_SERVER['MYSQL_PASSWORD'] ?? null;
        $dbname = $_SERVER['MYSQL_DATABASE'] ?? null;
        $port = $_SERVER['MYSQL_PORT'] ?? null;

        return new PDO("mysql:host=$host:$port;dbname=$dbname", $user, $password, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
    }

    /**
     * Close connection
     *
     * @param $connection
     */
    public function close(&$connection): void
    {
        $connection = null;
    }
}
