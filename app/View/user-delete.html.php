<?php
use App\Model\User;

$title = 'Users - Delete';

/** @var User $user */
$user = $arguments['user'];
?>

<div class="row justify-content-md-center align-items-center">
    <div class="col col-lg-10">
        <div class="card">
            <div class="card-header">
                User <?= htmlspecialchars($user->getName()) ?>
            </div>
            <div class="card-body">
                <p>
                Are you sure you want to delete this account belonging to <?= htmlspecialchars($user->getEmail()) ?>?
                </p>
                <form method="post">
                    <button type="submit" class="btn btn-danger">Delete</button>
                    <input type="hidden" name="token" value="<?= $_SESSION['token'] ?>">
                    <a class="btn btn-secondary" href="<?=$_SERVER["HTTP_REFERER"]?>">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
