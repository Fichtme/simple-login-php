<?php
use App\Model\User;

$title = 'Users';

/** @var User[] $users */
$users = $arguments['users'] ?? [];
?>

<div class="row justify-content-md-center align-items-center">
    <div class="col col-lg-10">
        <div class="card">
            <div class="card-header">
                Users
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
<!--                        <th scope="col">Enabled</th>-->
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $key => $user) : ?>
                        <tr>
                            <td><?= htmlspecialchars($user->getName()) ?></td>
                            <td><?= htmlspecialchars($user->getEmail())?></td>
<!--                            <td>--><?//= $user->isEnabled() ? "True" : "False" ?><!--</td>-->
                            <td>
                                <a href="?c=User&a=edit&user_id=<?=$user->getId()?>">Edit</a>
                                <a href="?c=User&a=delete&user_id=<?=$user->getId()?>">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="?c=User&a=create">Add</a>
            </div>
        </div>
    </div>
    <div class="col col-lg-2">
        <a class="btn btn-secondary" href="/?c=Login&a=logout&token=<?= $_SESSION['token'] ?>"> Logout </a>
    </div>
</div>