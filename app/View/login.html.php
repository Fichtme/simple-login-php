<?php $title = 'Login' ?>

<div class="row justify-content-md-center align-items-center">
    <div class="col col-lg-6">
        <div class="card">
            <div class="card-header">
                Login
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <input type="hidden" name="token" value="<?= $_SESSION['token'] ?>">

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-secondary" href="/?c=Login&a=register"> Register </a>
                </form>
            </div>
                <?php
                if (isset($arguments['errors'])) {
                    ?>
                    <div class="card-footer">
                    <?php
                    foreach ($arguments['errors'] as $error) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $error ?>
                        </div>
                        <?php
                    }
                    ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

