<?php $title = 'Users - Add' ?>

<div class="row justify-content-md-center align-items-center">
    <div class="col col-lg-6">
        <div class="card">
            <div class="card-header">
                Create user
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="form-group">
                        <label for="full-name">Full name</label>
                        <input type="text" name="full-name" class="form-control" id="full-name" aria-describedby="emailHelp" placeholder="Enter your full name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>

                    <div class="form-group">
                        <label for="repeatPassword">Repeat password</label>
                        <input type="password" name="password2" class="form-control" id="repeatPassword" placeholder="Repeat password">
                    </div>
                    <input type="hidden" name="token" value="<?= $_SESSION['token'] ?>">

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-secondary" href="/?c=User"> Back to users </a>
                </form>
            </div>
            <?php
            if (isset($arguments['errors'])) {
                ?>
                <div class="card-footer">
                    <?php
                    foreach ($arguments['errors'] as $error) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $error ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>