<?php

namespace App\Utils;

class Token
{
    public const TOKEN_LIFETIME = 900;

    /**
     * A new token is generated after using the function!
     *
     * @param string|null $token
     * @return bool
     */
    public static function isValid(?string $token = null): bool
    {
        if (!$token) {
            $token = $_POST['token'] ?? '';
        }
        if (hash_equals($_SESSION['token'], $token)
            && ((time() - $_SESSION['token_created']) <= self::TOKEN_LIFETIME)
        ) {
            self::generate(); #generate new token after handling 1 token
            return true;
        }
        self::generate();
        return false;
    }

    /**
     * @throws \Exception
     */
    public static function generate(): void
    {
        $_SESSION['token'] = bin2hex(random_bytes(32));
        $_SESSION['token_created'] = time();
    }

    /**
     * @return bool
     */
    public static function hasActiveToken(): bool
    {
        return !empty($_SESSION['token']) && ((time() - $_SESSION['token_created']) <= self::TOKEN_LIFETIME);
    }
}
