<?php

namespace App\Controller;

use App\Exception\RegisterException;
use App\Utils\Token;

/**
 * Class LoginController
 * @package App\Controller
 */
class LoginController extends AbstractController
{
    public function __invoke()
    {
        if ($this->userService->isLoggedIn()) {
            $this->redirect('?c=User');
        }

        if ($this->isMethod(self::REQUEST_POST)) {
            $username = $_POST['email'] ?? null;
            $passwordAttempt = $_POST['password'] ?? null;

            if (Token::isValid()) {
                if ($username && $passwordAttempt) {
                    $user = $this->userService->login($username, $passwordAttempt);
                    if ($user) {
                        $this->redirect('?c=User');
                    }
                }
                $errors[] = 'Oops! Invalid credentials or your account is inactive!';
            } else {
                $errors[] = "Token is invalid";
            }
        }

        $this->render('login.html.php', [
            'errors' => $errors ?? null
        ]);
    }

    public function registerAction()
    {
        if ($this->userService->isLoggedIn()) {
            $this->redirect('?c=User');
        }

        if ($this->isMethod(self::REQUEST_POST)) {
            $name = $_POST['full-name'] ?? null;
            $email = $_POST['email'] ?? null;
            $password = $_POST['password'] ?? null;
            $secondPassword = $_POST['password2'] ?? null;

            if ($password === $secondPassword) {
                if (Token::isValid()) {
                    try {
                        $this->userService->register($email, $name, $password);
                        $this->redirect('?c=Login');
                    } catch (RegisterException $exception) {
                        $errors[] = $exception->getMessage();
                    }
                } else {
                    $errors[] = "Token is invalid";
                }
            } else {
                $errors[] = 'Oops! Passwords do not match!';
            }
        }

        $this->render('register.html.php', [
            'errors' => $errors ?? null
        ]);
    }

    public function logoutAction()
    {
        if (Token::isValid($_GET['token'])) {
            $this->userService->logout();
            $this->redirect();
        }

        $this->redirect($_SERVER["HTTP_REFERER"]);
    }
}
