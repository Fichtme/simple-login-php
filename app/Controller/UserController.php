<?php

namespace App\Controller;

use App\Exception\RegisterException;
use App\Service\UserService;
use App\Utils\Token;

class UserController extends AbstractController
{
    public function __invoke()
    {
        $this->needsToBeLoggedIn();

        $this->render('users.html.php', [
            'users' => $this->userService->getAll()
        ]);
    }

    public function createAction()
    {
        $this->needsToBeLoggedIn();

        if ($this->isMethod(self::REQUEST_POST)) {
            $name = $_POST['full-name'] ?? null;
            $email = $_POST['email'] ?? null;
            $password = $_POST['password'] ?? null;
            $secondPassword = $_POST['password2'] ?? null;

            if ($password === $secondPassword) {
                if (Token::isValid()) {
                    try {
                        $this->userService->register($email, $name, $password);
                        $this->redirect('?c=User');
                    } catch (RegisterException $exception) {
                        $error[] = $exception->getMessage();
                    }
                } else {
                    $errors[] = "Token is invalid";
                }
            } else {
                $error[] = 'Oops! Passwords do not match!';
            }
        }

        $this->render('user-add.html.php', ['errors' => $errors ?? [],]);
    }

    public function editAction()
    {
        $this->needsToBeLoggedIn();

        $user = $this->userService->get($_GET['user_id']);

        if (!$user) {
            $this->redirect('?c=user');
        }

        if ($this->isMethod(self::REQUEST_POST)) {
            $name = $_POST['full-name'] ?? null;
            $email = $_POST['email'] ?? null;
            if (Token::isValid()) {
                $user = $this->userService->update($user, [
                    'name' => strip_tags($name),
                    'email' => strip_tags($email),
                ]);
            } else {
                $errors[] = "Token is invalid";
            }
        }

        $this->render('user-edit.html.php', [
            'user' => $user,
            'errors' => $errors ?? [],
        ]);
    }

    public function deleteAction()
    {
        $this->needsToBeLoggedIn();

        $user = $this->userService->get($_GET['user_id']);

        if (!$user) {
            $this->redirect('?c=user');
        }

        if ($this->isMethod(self::REQUEST_POST)) {
            if (Token::isValid()) {
                $deleted = $this->userService->delete($user);
                if ($deleted) {
                    $this->redirect('?c=user');
                }
            } else {
                $errors[] = "Token is invalid";
            }
        }

        $this->render('user-delete.html.php', [
            'user' => $user,
            'errors' => $errors ?? []
        ]);
    }
}
