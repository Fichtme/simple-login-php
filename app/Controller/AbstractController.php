<?php

namespace App\Controller;

use App\Service\UserService;

/**
 * Class AbstractController
 * @package App\Controller
 */
abstract class AbstractController
{
    public const REQUEST_GET = 'GET';
    public const REQUEST_POST = 'POST';

    /**
     * @var UserService
     */
    public UserService $userService;

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->userService = new UserService();
    }

    /**
     * @param string $template
     * @param array $arguments
     */
    public function render(string $template, array $arguments = [])
    {
        ob_start();
        require_once VIEW_PATH . $template;
        $content = ob_get_clean();
        require_once VIEW_PATH . 'base.html.php';
    }

    /**
     * @param string $string
     * @return bool
     */
    public function isMethod(string $string): bool
    {
        return $string === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @param string $url
     * @param int $statusCode
     */
    public function redirect(string $url = '', int $statusCode = 303): void
    {
        if ((strpos($url, 'http') === 0)) {
            header('Location: '. $url, true, $statusCode);
            exit();
        }
        header('Location: /' . $url, true, $statusCode);
        exit();
    }

    public function needsToBeLoggedIn()
    {
        if (!$this->userService->isLoggedIn()) {
            $this->redirect('?c=login');
        }

        if (!$this->userService->me()) {
            $this->userService->logout();
        }
    }
}
