<?php

require_once '../App.php';

function dd(...$args)
{
    echo '<pre>';
    foreach ($args as $value) {
        var_dump($value);
    }
    exit(1);
}

App::run();
