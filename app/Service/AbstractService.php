<?php

namespace App\Service;

use App\Manager\MysqlManager;
use App\Model\User;

/**
 * Class AbstractService
 * @package App\Service
 */
abstract class AbstractService
{
    /**
     * @var MysqlManager
     */
    protected MysqlManager $manager;

    public function __construct()
    {
        $this->manager = new MysqlManager();
    }

    abstract public function getTableName(): string;

    abstract public function getModelClass(): string;

    /**
     * Returns single result
     *
     * @param array $properties
     * @return null|object
     */
    public function findByProperties(array $properties): ?object
    {
        $result = $this->prepareStatement($properties)->fetch();
        if ($result) {
            return $result;
        }
        return null;
    }

    /**
     * Fetches single result from the database by id
     *
     * @param string $id
     * @return object|null
     */
    public function get(string $id): ?object
    {
        $result = $this->prepareStatement(['id' => $id])->fetch();
        if ($result) {
            return $result;
        }

        return null;
    }

    /**
     * Returns all results
     *
     * @param array $properties
     * @return array
     */
    public function getByProperties(array $properties): ?array
    {
        $result = $this->prepareStatement($properties)->fetchAll();
        if ($result) {
            return $result;
        }
        return [];
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $result = $this->prepareStatement()->fetchAll();
        if ($result) {
            return $result;
        }
        return [];
    }

    /**
     * @param array $properties
     * @return bool|\PDOStatement
     */
    private function prepareStatement(array $properties = [])
    {
        $connection = $this->manager->connect();

        $table = $this->getTableName();

        $query = "SELECT * FROM {$table} WHERE 1=1 ";

        foreach ($properties as $field => $value) {
            $query .= "AND $field=:$field ";
        }

        $stmt = $connection->prepare($query);

        foreach ($properties as $field => $value) {
            $stmt->bindParam($field, $value);
        }

        $stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getModelClass());
        $stmt->execute();
        return $stmt;
    }

    /**
     * @param string|object $id
     * @return bool
     */
    public function delete($id): bool
    {
        if (is_object($id)) {
            $id = $id->getId();
        }

        $connection = $this->manager->connect();
        $table = $this->getTableName();

        $query = "DELETE FROM {$table} WHERE id=:id";

        $stmt = $connection->prepare($query);
        $stmt->bindParam('id', $id);

        $stmt->execute();
        return $stmt->rowCount() === 1;
    }

    /**
     * @param $id
     * @param array $updates
     * @return object|null
     */
    public function update($id, $updates = []): ?object
    {
        if (is_object($id)) {
            $id = $id->getId();
        }

        $connection = $this->manager->connect();
        $table = $this->getTableName();

        $set = '';
        foreach ($updates as $field => $value) {
            $set .= ' `'.$field. '`=:'.$field.', ';
        }
        // remove last 2 characters from set
        $set = substr($set, 0, -2);
        $query = "UPDATE {$table} SET {$set} WHERE id = :id LIMIT 1";

        $stmt = $connection->prepare($query);

        $updates['id'] = $id;

        if ($stmt->execute($updates)) {
            return $this->get($id);
        }

        return null;
    }
}
