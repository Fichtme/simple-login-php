<?php

namespace App\Service;

use App\Exception\RegisterException;
use App\Model\User;

/**
 * Class UserService
 * @package App\Service
 */
class UserService extends AbstractService
{
    public function getTableName(): string
    {
        return 'app_user';
    }

    public function getModelClass(): string
    {
        return User::class;
    }

    /**
     * @param string $mail
     * @param string $password
     *
     * return array (user data)
     * @return bool
     */
    public function login(string $mail, string $password)
    {
        $mail = trim($mail);
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        $passwordAttempt = trim($password);

        /** @var User $user */
        $user = $this->findByProperties([
            'email' => $mail
        ]);

        if ($user && $user->isEnabled()) {
            $validPassword = password_verify($passwordAttempt, $user->getPassword());
            if ($validPassword) {
                $_SESSION['user_id'] = $user->getId();
                $_SESSION['logged_in'] = time();
                return true;
            }
        }

        return false;
    }

    /**
     * Register a user
     *
     * @param string $mail
     * @param string $name
     * @param string $password
     *
     * @throws RegisterException
     */
    public function register(string $mail, string $name, string $password)
    {
        $mail = strip_tags($mail);
        $name = strip_tags($name);
        $password = trim($password);

        # Dont validate if an name is actually a name
//        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
//            throw new RegisterException('Oops! Are you sure that\'s your name?');
//        }
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            throw new RegisterException('Oops! That is not an email!');
        }
        if (strlen($password) < 8) {
            throw new RegisterException('Oops! That password is a bit short! Use a minimal of 8 characters!');
        }

        $connection = $this->manager->connect();
        $sql = "SELECT COUNT(email) AS count FROM app_user WHERE email = :email";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue(':email', $mail);
        $stmt->execute();

        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($row['count'] > 0) {
            throw new RegisterException('Oops! That email already exists!');
        }

        $passwordHash = $this->hashPassword($password);
        $sql = "INSERT INTO 
                    app_user (id, name, email, password, enabled) 
                VALUES (UUID(), :name, :email, :password, 1)";
        $stmt = $connection->prepare($sql);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':email', $mail);
        $stmt->bindValue(':password', $passwordHash);
        $result = $stmt->execute();

        if (!$result) {
            throw new RegisterException('Oops! Something went wrong while signing up!');
        }

        return $result;
    }

    private function hashPassword(string $password)
    {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
    }

    /**
     * Check if user id is in the current session and if user has been logged in for less then 15 minutes
     *
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        $loggedIn = $_SESSION['user_id'] !== null && ((time() - $_SESSION['logged_in']) <= 900);

        if ($loggedIn) {
            $_SESSION['logged_in'] = time();
        }

        return $loggedIn;
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        session_destroy();
    }

    /**
     * @return User|null
     */
    public function me(): ?User
    {
        /** @var null|User $user */
        $user = $this->get($_SESSION['user_id']);
        if ($user && $user->isEnabled()) {
            return $user;
        }

        return null;
    }
}
