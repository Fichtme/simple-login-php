<?php

use App\Utils\Token;

/**
 * Class Application
 */
class App
{
    /**
     * Start the application
     */
    public static function run(): void
    {
        self::init();
        self::autoload();
        self::startSession();
        self::dispatch();
    }

    private static function init(): void
    {
        define("DS", DIRECTORY_SEPARATOR);
        define("PUBLIC+PATH", getcwd() . DS);
        chdir('../');
        define("APP_PATH", getcwd() . DS);
        define("CONTROLLER_PATH", APP_PATH . "Controller" . DS);
        define("SERVICE_PATH", APP_PATH . "Service" . DS);
        define("VIEW_PATH", APP_PATH . "View" . DS);

        define("CONTROLLER", $_REQUEST['c'] ?? 'Login');
        define("ACTION", $_REQUEST['a'] ?? null);
    }

    /**
     * Autoload
     */
    private static function autoload(): void
    {
        spl_autoload_register([__CLASS__,'load']);
    }

    /**
     * @param string $classname
     */
    private static function load(string $classname): void
    {
        $pos = strpos($classname, 'App\\');
        if ($pos !== false) {
            $classname = substr_replace($classname, '', $pos, strlen('App\\'));
            $classname = str_replace('\\', '/', $classname);
        } else {
            throw new RuntimeException('Something went wrong!');
        }
        $file = APP_PATH . "$classname.php";
        if (is_file($file)) {
            require_once APP_PATH . "$classname.php";
        } else {
            self::return404();
            throw new \Exception(highlight_string(sprintf('Could not load class %s', $classname)));
        }
    }

    private static function startSession()
    {
        session_start();
        if (!Token::hasActiveToken()) {
            Token::generate();
        }
    }

    private static function dispatch(): void
    {
        $controllerClass = "\App\Controller\\".CONTROLLER . "Controller";

        if (!class_exists($controllerClass)) {
            self::return404();
        }

        $controller = new $controllerClass();
        $action = ACTION . "Action";

        if (!ACTION && is_callable($controller)) {
            $controller();
        } elseif (is_callable([$controller, $action])) {
            $controller->$action();
        } else {
            self::return404();
        }
    }

    private static function return404(): void
    {
        http_response_code(404);
        exit(1);
    }
}
