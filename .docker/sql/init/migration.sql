/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `app_user`;

CREATE TABLE `app_user` (
                            `id` char(36) CHARACTER SET utf8 NOT NULL DEFAULT '',
                            `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
                            `password` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
                            `email` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
                            `enabled` tinyint(1) NOT NULL DEFAULT '1',
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `app_user` WRITE;
/*!40000 ALTER TABLE `app_user` DISABLE KEYS */;

INSERT INTO `app_user` (`id`, `name`, `password`, `email`, `enabled`)
VALUES
('0193240b-cf6d-11ea-8131-0242c0a89002', 'Henny Krijnen 4', '$2y$12$4XHcDjkKhmiRVIhb4XwrPOeTpBtFYXP4E066V695B9cVIZHjufbiO', 'krijnen.h+3@gmail.com', 1),
('095b5fa1-cf6c-11ea-8131-0242c0a89002', 'Henny Krijnen', '$2y$12$2anFFa6uXyU/VzHnd5yqmuJcA3e7unYw8OEv2464TrtvVhqKMlxya', 'krijnen.h@gmail.com', 1),
('b3b61978-cf6d-11ea-8131-0242c0a89002', 'Henny', '$2y$12$0SlR9Jlt/zoe6eI8BRxLVuOP8SYvJn1EMudm/cADnofqZzO34zosy', 'henny@fichtme.nl', 1),
('ecc282e7-cf6f-11ea-8892-0242c0a89003', 'Test user', '$2y$12$Z.exh6TTWExMpqQgDHA91.imNxIvp1GxlakeuGokTD1PQsTkx.gry', 'test@example.com', 1),
('f6b60942-cf6c-11ea-8131-0242c0a89002', 'Henny Krijnen', '$2y$12$rTAeuQYBFnFl6mNlPyZQb.DxH0ddPWZznB4k4oSAY9RBqumHEwvSu', 'krijnen.h+2@gmail.com', 1);


/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
