#!/bin/bash
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

docker_wait() {
  START=$(date +%s)
  echo "Fichtme - PHP | Scanning $1 on port $2..."
  while ! nc -z -v $1 $2;
    do
    if [[ $(($(date +%s) - $START)) -gt ${MAX_EXECUTION_TIME:=300} ]]; then
        echo "Fichtme - PHP | Service $1 on port $2 did not start or could not be reached within ${MAX_EXECUTION_TIME:=300} seconds. Aborting..."
        exit 1
    fi
    echo "Fichtme - PHP | Retry scanning $1 on port $2 in ${SCAN_INTERVAL:=2} seconds..."
    sleep ${SCAN_INTERVAL:=2}
  done
}

PHP=`which php`

echo "Fichtme - PHP | -------------------------"
if [[ -n "$MYSQL_HOST" ]]; then
echo "Fichtme - PHP | DB_HOST: $MYSQL_HOST":${MYSQL_PORT:=3306}
fi
echo "Fichtme - PHP | PHP Version: $(php -r 'echo PHP_VERSION;')"
echo "Fichtme - PHP | -------------------------"

if [[ -n "$MYSQL_HOST" ]]; then
    docker_wait $MYSQL_HOST ${MYSQL_PORT:=3306}
    echo "Fichtme - PHP | DB is up"
fi

echo "Fichtme - PHP | Ready"
exec docker-php-entrypoint "$@"


