# Simple login system
A plain php login system without a framework or any other package

## What?
1. Create a login page where you can sing in with the correct username and password combination;
1. After logging in you'll be redirected to a page where you can find a table with all the users
    - You can update a user
    - You can delete a user
    - A user has an email, a name, and a password
1. Layout is not important
1. Don't use a framework or other packages;
1. Take into account the OWASP list.

## local installation
Duplicate & configure your .env file
```console
cp .env.dist .env
```

Build Docker
```bash
docker-compose build
```

Start Docker
```bash
docker-compose up
```

Wait for it to start up and visit http://localhost:8080

Login: 

- Username: test@example.com
- Password: example123
